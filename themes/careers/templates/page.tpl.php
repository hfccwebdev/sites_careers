<?php

/**
 * @file
 * Theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 */
?>
  <div id="page">
    <!-- HEADER -->
    <header id="section-header" class="clearfix">
      <?php if ($secondary_menu): ?>
        <div id="section-secondary-navigation">
          <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('inline')))); ?>
          <?php if (!empty($page['header'])): ?>
            <?php print render($page['header']); ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['highlighted'])): ?>
        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php print $messages; ?>
      <div class="header-container">
        <?php if ($site_name || $site_slogan): ?>
          <div id="name-and-slogan" class="clearfix">
            <?php if ($site_name): ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </h1>
            <?php endif; ?>
            <?php if ($site_slogan): ?>
              <div id="site-slogan"><?php print $site_slogan; ?></div>
            <?php endif; ?>
          </div> <!-- /#name-and-slogan -->
        <?php endif; ?>
        <!-- Main Nav -->
        <nav id="section-main-navigation" class="clearfix">
          <div class="site-logo">
            <div id="site-logo">
              <a class="hfcc-home no-link" href="<?php print $front_page ?>">
                <img src="<?php print $logo ?>" alt="Henry Ford College <?php print $site_name ?>">
              </a>
            </div>
          </div>
          <div id="main-navigation">
            <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('inline')))); ?>
            <?php if (!empty($page['navigation'])): ?>
              <?php print render($page['navigation']); ?>
            <?php endif; ?>
          </div>
        </nav> <!-- /#section-main-navigation -->
      </div>
    </header>
    <!-- Billboard -->
    <section id="section-billboard">
      <div id="billboard" class="region-billboard">
        <?php if (!empty($page['billboard'])): ?>
        <?php print render($page['billboard']); ?>
        <?php endif; ?>
      </div>
        <?php if ($breadcrumb): ?><div id="breadcrumb" class="no-link"><?php print $breadcrumb; ?></div><?php endif; ?>
    </section> <!-- /#section-billboard -->
    <!-- MAIN -->
    <section id="section-main">
      <div id="main-inner" class="clearfix">
        <div id="content" class="column">
          <div id="content-inner" class="clearfix">
            <a id="main-content"></a>
            <?php print render($title_prefix); ?>
            <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
            <?php print render($title_suffix); ?>
            <?php print render($page['help']); ?>
            <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
            <?php print render($page['content']); ?>
            <?php print $feed_icons; ?>
          </div>
        </div>
        <!-- Front News Events -->
        <?php if (!empty($page['news'])): ?>
        <div id="news-contact">
          <div class="content highlight-header">
            <div class="flex-wrap">
              <div id="news">
                <?php print render($page['news']); ?>
              </div>
              <div id="contact">
                <?php print render($page['contact']); ?>
              </div>
            </div>
          </div>
        </div> <!--/#news-events -->
        <?php endif; ?>
      </div>
      <!-- FIRST SIDEBAR -->
      <?php if (!empty($page['sidebar_first'])): ?>
        <aside id="sidebar-first" class="column sidebar">
          <?php print render($page['sidebar_first']); ?>
        </aside> <!-- /#sidebear-first -->
      <?php endif; ?>
      <!-- SECOND SIDEBAR -->
      <?php if (!empty($page['sidebar_second'])): ?>
        <aside id="sidebar-second" class="column sidebar">
          <?php print render($page['sidebar_second']); ?>
        </aside> <!-- /#sidebar-second -->
      <?php endif; ?>
    </section> <!-- /#main-inner, /#section-main -->
    <!-- FATFOOTER -->
    <?php if (!empty($page['fatfooter'])): ?>
      <footer id="section-fatfooter" class="clearfix">
        <?php print render($page['fatfooter']); ?>
      </footer>
    <?php endif; ?>
    <!-- FOOTER -->
    <?php if (!empty($page['footer'])): ?>
      <footer id="section-footer" class="clearfix">
        <?php print render($page['footer']); ?>
      </footer>
    <?php endif; ?>
  </div> <!-- /#page -->
